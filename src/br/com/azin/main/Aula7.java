package br.com.azin.main;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import br.com.azin.model.Venda;

public class Aula7 {
	public static void main(String[] args) throws Exception {
		JAXBContext contexto = JAXBContext.newInstance(Venda.class);
		Unmarshaller unmarshaller = contexto.createUnmarshaller();
		Venda venda = (Venda) unmarshaller.unmarshal(new File("src/vendas.xml"));
		System.out.println(venda);
	}
}
