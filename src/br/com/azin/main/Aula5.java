package br.com.azin.main;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import br.com.azin.model.Produto;

public class Aula5 {

	public static void main(String[] args) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(true);
		factory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage",
				"http://www.w3.org/2001/XMLSchema");
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document document = builder.parse("src/vendas.xml");

		String moeda = document.getDocumentElement().getAttribute("moeda");
		System.out.println(moeda);

		NodeList tagFormaDePagamento = document.getElementsByTagName("formaDePagamento");
		Element item = (Element) tagFormaDePagamento.item(0);
		String formaDePagamento = item.getTextContent();

		System.out.println(formaDePagamento);

		XPath xPath = XPathFactory.newInstance().newXPath();

	    String expression = "/venda/produtos/produto[1]";

	    XPathExpression xPathExpression = xPath.compile(expression);
	    NodeList produtos = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
		for (int i = 0; i < produtos.getLength(); i++) {
			Element produtoTag = (Element) produtos.item(i);
			String nome = produtoTag.getElementsByTagName("nome").item(0).getTextContent();
			Double preco = Double.parseDouble(produtoTag.getElementsByTagName("preco").item(0).getTextContent());

			Produto produto = new Produto(nome, preco);
			System.out.println(produto);
		}

	}

}
